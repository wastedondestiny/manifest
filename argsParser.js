'use strict';

const chalk = require('chalk')
const meow = require('meow')

const help = chalk`
Usage
$ {cyan node main.js}

Options
{green --adapter, -a}  Adapter name
{green --key, -k}  Destiny API key
{green --force, -f}  Force manifest conversion to output file
{green --output, -o}  Output file name`

const cli = meow(help, {
  flags: {
    adapter: {
      type: 'string',
      alias: 'a'
    },
    key: {
      type: 'string',
      alias: 'k'
    },
    force: {
      type: 'boolean',
      alias: 'f',
      default: false
    },
    output: {
      type: 'string',
      alias: 'o'
    }
  }
})

module.exports = {
  parse: () => cli.flags
}
