'use strict';

const dotenv = require('dotenv')
const argsParser = require('./argsParser')
const fileManager = require('./fileManager')
const manifestLoader = require('./manifestLoader')

dotenv.config()

module.exports = {
  run: async () => {
    let { adapter, key, force, output } = argsParser.parse()

    if (!key) key = process.env.APIKEY

    if (!adapter || !key || !output) {
      throw new Error('Invalid parameters')
    }
    
    await fileManager.createDirectories()

    const adapterModule = require(`./${adapter}.js`)
    const manifest = await manifestLoader.load(key)
    const worldContents = new Map()
    const cacheFileName = `${adapter}.${manifest.Response.version}.version`
    const isCached = await fileManager.cacheFileExists(cacheFileName)

    for (const locale of Object.keys(manifest.Response.mobileWorldContentPaths)) {
      worldContents.set(locale, await manifestLoader.getWorldContents(manifest, key, locale))
    }
    
    if (force || !isCached) {
      await fileManager.writeJsonAndIgnoreCache(output, () => adapterModule.execute(key, worldContents))
    } else {
      await fileManager.writeJsonAndCache(output, () => adapterModule.execute(key, worldContents))
    }
    
    await fileManager.writeCacheFile(cacheFileName)
  }
}
