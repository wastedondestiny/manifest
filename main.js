#!/usr/bin/env node
'use strict';

const chalk = require('chalk')
const log = console.log
const app = require('./app')

;(async () => {
  try {
    await app.run()
    log(chalk`{green Script run successfully}`)
  } catch (error) {
    log(chalk`{red ${error.stack}}`)
    log(chalk`{cyan Type 'node main.js --help' to see usage}`)
  }
})()
