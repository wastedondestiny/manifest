'use strict'

const fetch = require('node-fetch')
const baseUri = 'https://www.bungie.net'

const fetchAbsolute = (url, ...args) => url.startsWith('/') ? fetch(baseUri + url, ...args) : fetch(url, ...args)
const get = async (url, key) => await fetchAbsolute(url, { headers: { 'x-api-key': key }})
const getJson = async (url, key) => await (await get(url, key)).json()

module.exports = {
  getJson,
  get
}
