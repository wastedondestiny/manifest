'use strict'

const fs = require('fs')
const sqlite3 = require('sqlite3').verbose()
const unzip = require('unzipper')
const binDir = 'bin/'
const buildDir = 'build/'

const unzipFile = (stream, fileName) => new Promise(resolve => {
  stream
    .pipe(unzip.Parse())
    .on('entry', entry => {
    entry
      .pipe(fs.createWriteStream(fileName))
      .on('finish', () => {
      resolve()
    })
  })
})

const fileExists = fileName => new Promise(resolve => 
  fs.access(fileName, error => {
    if (error) return resolve(false)
    resolve(true)
  }))

const readFile = fileName => new Promise((resolve, reject) =>
  fs.readFile(fileName, (error, data) => {
    if (error) return reject(error)
    resolve(data)
  }))

const writeFile = (fileName, data) => new Promise((resolve, reject) =>
  fs.writeFile(fileName, data, error => {
    if (error) return reject(error)
    resolve()
  }))

module.exports = {
  createDirectories: async () => {
    if (!await fileExists(binDir.slice(0, -1))) fs.mkdirSync(binDir.slice(0, -1))
    if (!await fileExists(buildDir.slice(0, -1))) fs.mkdirSync(buildDir.slice(0, -1))
  },
  databaseExists: fileName => {
    return fileExists(binDir + fileName)
  },
  cacheFileExists: fileName => {
    return fileExists(buildDir + fileName)
  },
  writeCacheFile: fileName => {
    return writeFile(buildDir + fileName, Buffer.alloc(0))
  },
  writeJsonAndIgnoreCache: async (fileName, getFileCallback) => {
    const file = await getFileCallback()
    await writeFile(buildDir + fileName, JSON.stringify(file))
    return file
  },
  writeJsonAndCache: async (fileName, getFileCallback) => {
    if (await fileExists(buildDir + fileName)) {
      return JSON.parse(await readFile(buildDir + fileName))
    } else {
      const file = await getFileCallback()
      await writeFile(buildDir + fileName, JSON.stringify(file))
      return file
    }
  },
  readDatabaseAndCache: async (fileName, getFileCallback) => {
    if (await fileExists(binDir + fileName)) {
      return new sqlite3.Database(binDir + fileName)
    } else {
      const file = await getFileCallback()
      await unzipFile(file.body, binDir + fileName)
      return new sqlite3.Database(binDir + fileName)
    }
  }
}
