'use strict'

const getAll = (db, dataSet) => new Promise((resolve, reject) => {
  db.all(`SELECT * FROM ${dataSet}`, [], (error, rows) => {
    if (error) {
      reject(error)
    }
    resolve(rows.map(row => JSON.parse(row.json)))
  })
})

const extractActivities = async worldContents => {
  const activities = new Map()
  const definitions = await getAll(worldContents, 'DestinyActivityDefinition')

  if (!definitions) {
    throw new Error('Could not find any activity definition in the world contents')
  }

  for (let definition of definitions) {
    activities.set(definition.hash, {
      modeHash: definition.directActivityModeHash || '',
      name: definition.displayProperties.name || ''
    })
  }

  return activities
}

const extractActivityModes = async worldContents => {
  const activityModes = new Map()
  const definitions = await getAll(worldContents, 'DestinyActivityModeDefinition')

  if (!definitions) {
    throw new Error('Could not find any activity mode definition in the world contents')
  }

  for (let definition of definitions) {
    activityModes.set(definition.hash, {
      name: definition.displayProperties.name || ''
    })
  }

  return activityModes
}

const buildActivityEntries = (activities, modes) => {
  const entries = {}
  const englishActivities = activities.get('en')

  for (const key of englishActivities.keys()) {
    entries[key] = {}

    for (const locale of activities.keys()) {
      const activity = activities.get(locale).get(key)
      const mode = modes.get(locale).get(activity.modeHash)
      entries[key][locale] = {
        name: activity.name,
        mode: mode ? mode.name : ''
      }
    }
  }

  return entries
}

module.exports = {
  execute: async (key, worldContents) => {
    const activities = new Map()
    const modes = new Map()

    for (const [locale, localizedWorldContents] of worldContents) {
      activities.set(locale, await extractActivities(localizedWorldContents))
      modes.set(locale, await extractActivityModes(localizedWorldContents))
    }

    return buildActivityEntries(activities, modes)
  }
}
