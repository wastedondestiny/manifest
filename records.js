'use strict'

const getAll = (db, dataSet) => new Promise((resolve, reject) => {
  db.all(`SELECT * FROM ${dataSet}`, [], (error, rows) => {
    if (error) {
      reject(error)
    }
    resolve(rows.map(row => JSON.parse(row.json)))
  })
})

const extractRecords = async worldContents => {
  const records = {}
  const definitions = new Map()

  const objectives = await getAll(worldContents.get('en'), 'DestinyObjectiveDefinition')
  
  for (const [locale, localizedWorldContents] of worldContents) {
    definitions.set(locale, await getAll(localizedWorldContents, 'DestinyRecordDefinition'))
  }

  const englishDefinitions = definitions.get('en')

  if (!englishDefinitions) {
    throw new Error('Could not find any record definition in the world contents')
  }

  for (let definition of englishDefinitions.filter(x => x.titleInfo && x.titleInfo.hasTitle)) {
    const englishTitle = englishDefinitions.find(x => x.hash === definition.hash).titleInfo
    records[definition.hash] = {
      gilded: englishTitle ? englishTitle.gildingTrackingRecordHash : null
    }

    for (const locale of definitions.keys()) {
      const title = definitions.get(locale).find(x => x.hash === definition.hash)
      records[definition.hash][locale] = title.titleInfo.titlesByGender['Male']
    }
  }

  return records
}

module.exports = {
  execute: async (key, worldContents) => {
    return await extractRecords(worldContents)
  }
}
