'use strict';

const api = require('./api')
const fileManager = require('./fileManager')

module.exports = {
  load: async (key) => {
    const manifest = await api.getJson('/Platform/Destiny2/Manifest/', key)

    if (!manifest || !manifest.Response) {
      throw new Error('Could not retrieve manifest')
    }

    return manifest
  },
  getWorldContents: async (manifest, key, locale) => {
    if (!manifest || !manifest.Response || !manifest.Response.mobileWorldContentPaths || !manifest.Response.mobileWorldContentPaths[locale]) {
      throw new Error(`Could not retrieve world contents for the locale '${locale}'`)
    }

    const fileName = `${manifest.Response.version}_${locale}.sqlite`
    const getFile = async () => await api.get(manifest.Response.mobileWorldContentPaths[locale], key)
    return await fileManager.readDatabaseAndCache(fileName, getFile)
  }
}
