'use strict'

const getAll = (db, dataSet) => new Promise((resolve, reject) => {
  db.all(`SELECT * FROM ${dataSet}`, [], (error, rows) => {
    if (error) {
      reject(error)
    }
    resolve(rows.map(row => JSON.parse(row.json)))
  })
})

const getTriumphNode = (db) => new Promise((resolve, reject) => {
  db.all(`SELECT DestinyPresentationNodeDefinition.json FROM DestinyPresentationNodeDefinition
          JOIN DestinyObjectiveDefinition
          ON json_extract(DestinyPresentationNodeDefinition.json, '$.objectiveHash') = json_extract(DestinyObjectiveDefinition.json, '$.hash')
          WHERE json_extract(DestinyPresentationNodeDefinition.json, '$.displayProperties.name') = 'Triumphs'
          AND json_extract(DestinyObjectiveDefinition.json, '$.scope') = 3`, [], (error, rows) => {
    if (error) {
      reject(error)
    }
    resolve(JSON.parse(rows[0].json))
  })
})

const extractSeasons = async worldContents => {
  const seasonSummaries = {}
  const seasons = new Map()
  const englishWorldContents = worldContents.get('en')

  for (const locale of worldContents.keys()) {
    seasons.set(locale, await getAll(worldContents.get(locale), 'DestinySeasonDefinition'))
  }

  const englishSeasons = seasons.get('en')
  const seasonPasses = await getAll(englishWorldContents, 'DestinySeasonPassDefinition')
  const triumphNode = await getTriumphNode(englishWorldContents)

  if (!englishSeasons) {
    throw new Error('Could not find any season definition in the world contents')
  }

  if (!seasonPasses) {
    throw new Error('Could not find any season pass definition in the world contents')
  }

  if (!triumphNode) {
    throw new Error('Could not find any triumph node definition in the world contents')
  }

  for (let season of englishSeasons.filter(x => x.seasonPassHash)) {
    if (!season.startDate || new Date(season.startDate) > new Date()) continue // Ignore future seasons
    const seasonPass = seasonPasses.find(x => x.hash === season.seasonPassHash)
    const name = {}
    
    for (const locale of seasons.keys()) {
      name[locale] = seasons.get(locale).find(x => x.hash === season.hash).displayProperties.name
    }

    seasonSummaries[season.hash] = {
      id: season.seasonNumber,
      name,
      rewardProgressionHash: seasonPass ? seasonPass.rewardProgressionHash : null,
      prestigeProgressionHash: seasonPass ? seasonPass.prestigeProgressionHash : null,
      maxTriumphScore: triumphNode.maxCategoryRecordScore
    }
  }

  return seasonSummaries
}

module.exports = {
  execute: async (key, worldContents) => {
    return await extractSeasons(worldContents)
  }
}
